package com.DataProcessing;


import javafx.stage.FileChooser;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import static java.lang.System.exit;

/**
 * Created by mabak on 07.07.2017.
 */
public class DataProcessor {

    private static final String FILE_NOT_VALID = "File not valid";
    private static final String DEFAULT_FILE_NAME = "coords.txt";
    private static final String DEFAULT_FILE_NAME2 = "coords2.txt";

    double avgLatitude = 0;
    double avgLongitude = 0;


    List<String> list = new ArrayList<>();
    List<Coordinates> listCoords = new ArrayList<>();



    public void readData() {
        String fileName = "";
        FileChooser file = new FileChooser();
        File selectedFile = file.showOpenDialog(null);

        if (selectedFile != null && selectedFile.getName().toString().equals(DEFAULT_FILE_NAME)) {

            fileName = selectedFile.toString();
        } else if (selectedFile != null && selectedFile.getName().toString().equals(DEFAULT_FILE_NAME2)) {
            fileName = selectedFile.toString();
        } else {
            System.out.println(FILE_NOT_VALID);
            exit(0);
        }

        try (BufferedReader br = Files.newBufferedReader(Paths.get(fileName))) {
            list = br.lines()
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }

        listCoords = list.stream()
                .map(string -> string.split(",", 2))
                .map(a -> new Coordinates(Double.parseDouble(a[0]), Double.parseDouble(a[1])))
                .collect(Collectors.toList());

        avgLatitude = listCoords.stream().mapToDouble(s -> s.getLatitude()).average().getAsDouble();
        avgLongitude = listCoords.stream().mapToDouble(s -> s.getLongitude()).average().getAsDouble();

    }
    public double getAvgLatitude() {
        return avgLatitude;
    }

    public double getAvgLongitude() {
        return avgLongitude;
    }

    public List<Coordinates> getListCoords() {
        return listCoords;
    }

}
