package com.DataProcessing;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;

/**
 * Created by mabak on 07.07.2017.
 */
public class Coordinates {
    private final DoubleProperty latitude;
    private final DoubleProperty longitude;

    public Coordinates(double latitude, double longitude){
        this.latitude=new SimpleDoubleProperty(latitude);
        this.longitude=new SimpleDoubleProperty(longitude);
    }

    public double getLatitude() {
        return latitude.getValue();
    }

    public double getLongitude() {
        return longitude.getValue();
    }

    public DoubleProperty latitudeProperty(){
        return latitude;
    }
    public DoubleProperty longitudeProperty(){
        return longitude;
    }


}