package com;

import com.DataProcessing.DataProcessor;
import com.DataProcessing.Coordinates;
import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.MapComponentInitializedListener;
import com.lynden.gmapsfx.javascript.object.*;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

public class Controller implements Initializable, MapComponentInitializedListener {

    private DataProcessor dataProcessor = new DataProcessor();

    private ObservableList<Coordinates> coordsData = FXCollections.observableArrayList();
    private List<Coordinates> coordsList;

    public double avgLat = 0;
    public double avgLong = 0;


    @FXML
    private TableView<Coordinates> myTable;
    @FXML
    private TableColumn<Coordinates, Number> myTableLat;
    @FXML
    private TableColumn<Coordinates, Number> myTableLong;



    @FXML
    void handleButtonAction(ActionEvent e) {
        dataProcessor.readData();
        reloadMarkers();
        myTableLat.setCellValueFactory(cellData -> cellData.getValue().latitudeProperty());
        myTableLong.setCellValueFactory(cellData -> cellData.getValue().longitudeProperty());
        myTable.setItems(getCoordsData());
        coordsList.forEach(s -> addCoordsData(s));

    }



    @FXML
    private void closeButtonAction() {
        Platform.exit();
        System.exit(0);
    }

    @FXML
    private GoogleMapView mapView;
    private GoogleMap map;


    @Override
    public void initialize(URL url, ResourceBundle rb) {
        mapView.addMapInializedListener(this);

    }

    public void reloadMarkers() {
        coordsList = dataProcessor.getListCoords();
        coordsList.forEach(s -> map.addMarker((new Marker(new MarkerOptions().position(new LatLong(s.getLatitude(), s.getLongitude()))))));
        avgLat = dataProcessor.getAvgLatitude();
        avgLong = dataProcessor.getAvgLongitude();
        map.setCenter(new LatLong(avgLat, avgLong));
    }

    @Override
    public void mapInitialized() {

        MapOptions mapOptions = new MapOptions();
        mapOptions.center(new LatLong(avgLat, avgLong))
                .mapType(MapTypeIdEnum.ROADMAP)
                .overviewMapControl(false)
                .panControl(false)
                .rotateControl(false)
                .scaleControl(false)
                .streetViewControl(false)
                .zoomControl(false)
                .zoom(5);

        map = mapView.createMap(mapOptions);
    }
    public void addCoordsData(Coordinates mc){
        coordsData.add(mc);

    }
    public ObservableList<Coordinates> getCoordsData(){
        return coordsData;
    }
}