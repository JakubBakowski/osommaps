# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This application is used for showing coordinates retrieved from *.txt file.

### How do I get set up? ###

Pull the project from develop branch, open it in Your favourite IDE. Build the project and run it.
Sample data can be found here : https://drive.google.com/open?id=0B7KUOIlmyzGIUG90NFYxSXFFdUk .
I used IntelliJ and for some reason it sometimes deleted Scene.fxml file. If that happens just copy it from src\main\java\com to target\classes\com.

### Used frameworks ###
JavaFX, JFoenix, GMapsFX